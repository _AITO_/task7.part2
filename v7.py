# For the given integer X print 1 if it's positive, -1 if it's negative, or 0 if it's equal to zero
# Try to use the cascade if-elif-else for it

n = int (input("Введите число:  "))
if n > 0:
    print(1)
elif n == 0:
    print(0)
elif n < 0:
    print(-1)